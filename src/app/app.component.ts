import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  search = "";
  data = [
    "samsung",
    "dell",
    "Apple",
    "Hwawui",
    "motorolla",
    "sony",
    "alicatel",
    "techno"
  ];
  filtered = [];
  selectedItem = "";
  constructor() {}

  ngOnInit() {}

  doSearch(e) {
    const value = e.target.value;
    this.filtered = this.data.filter(
      item => item.toLowerCase().indexOf(value.toLocaleLowerCase()) > -1
    );
  }

  selectItem(e) {
    this.selectedItem = e;
  }
}

